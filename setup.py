from setuptools import setup, find_packages

setup(
    name="fina",
    version="1",
    packages=find_packages(),
    entry_points={
        "console_scripts": ["fina = fina.application:main", "finamon = fina.monitor:monitor"]
        },
    install_requires=[
        'flask>=0.12.1',
        'flask_restful>=0.3.6',
        'RPi.GPIO>=0.6.3'
        ]
    )
