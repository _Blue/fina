import sys
import RPi.GPIO as gpio
import time
from threading import Thread
from . import config

class Sensor:
    thread = None

    def __init__(self, on_signal):
        self.on_signal = on_signal
        gpio.setmode(gpio.BOARD)
        for e in config.sensors:
            gpio.setup(e, gpio.IN)

    def start_impl(self):
        while True:
            if self.one_sensor_triggered():
                self.on_signal()

            time.sleep(config.sensor_wait)

    # Check if sensors has been triggered
    def triggered(id: int) -> bool:
        return gpio.input(id) == 1

    def one_sensor_triggered(self):
        return any(Sensor.triggered(e) for e in config.sensors)

    def start(self):
        assert not self.thread
        self.thread = Thread(target=Sensor.start_impl, args=[self])
        self.thread.start()
