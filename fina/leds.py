import time

from enum import Enum
from threading import Thread, Event
from .state_handler import State
from . import config

class LedStatus(Enum):
    On = 0
    Off = 1
    Blinking = 2

# State -> (green led, red led)

state_map = {
              State.Opened: (LedStatus.On, LedStatus.Off),
              State.Locking: (LedStatus.Blinking, LedStatus.Off),
              State.Locked: (LedStatus.On, LedStatus.On),
              State.Triggered: (LedStatus.On, LedStatus.Blinking),
              State.Alerted: (LedStatus.Off, LedStatus.Blinking)
            }

class Led:
    state = None
    last_state = None
    flag = True

    def __init__(self, path: str):
        self.path = path

    def transition(self, state):
        self.state = state

    def write_state(self, flag: bool):
        if self.last_state is not None and flag == self.last_state:
            return

        with open(self.path, "w") as fd:
            fd.write("1" if flag else "0")

        self.last_state = flag

    def tick(self):
        if self.state == LedStatus.Blinking:
            self.flag = not self.flag
        else:
            self.flag = self.state == LedStatus.On

        self.write_state(self.flag)


class Leds:
    state = None
    thread = None
    signaled = False
    event = Event()
    leds = [Led(config.green_led_file), Led(config.red_led_file)]

    def on_state_change(self, state):
        self.state = state

        colors = state_map[state]

        self.leds[0].transition(colors[0])
        self.leds[1].transition(colors[1])

    def on_signal(self):
        self.event.set()

    def run(self):
        signaled = False

        while True:
            for i, e in enumerate(self.leds):
                if signaled and i == 1:
                    e.write_state(True)
                    signaled = False
                else:
                    e.tick()

            if self.event.wait(config.led_timeout):
                self.event.clear()
                signaled = True

    def start(self):
        assert not self.thread

        # Initialize leds in 'opened state'
        # Toggle the leds first, since leaving a led unset might be ignored by the kernel
        for e in self.leds:
            e.write_state(True)
            e.write_state(False)

        # Sad sleep to work around race condition on rpi 3B firmware
        time.sleep(1)
        self.on_state_change(State.Opened)

        self.thread = Thread(target=Leds.run, args=[self])
        self.thread.start()
