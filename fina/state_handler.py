from enum import Enum
from PyQt5.QtCore import *
import sys

class AlarmState():
    def __init__(self, name: str, front_color, password_input: bool):
        self.name = name
        self.color = front_color
        self.password_input = password_input

    @property
    def Name(self):
        return self.name

    @property
    def Color(self):
        return self.color

    @property
    def RequirePassword(self):
        return self.password_input

    def __eq__(self, other):
        return self.name == other.Name


class State(Enum):
    Opened = AlarmState("Opened", Qt.darkGreen, False)
    Locking = AlarmState("Locking", Qt.darkBlue, True)
    Locked = AlarmState("Locked", Qt.blue, True)
    Triggered = AlarmState("Triggered", Qt.yellow, True)
    Alerted = AlarmState("Alerted", Qt.red, True)


transitions = [
                (State.Opened, State.Locking),
                (State.Locking, State.Opened),
                (State.Locking, State.Locked),
                (State.Locking, State.Triggered),
                (State.Locked, State.Opened),
                (State.Locked, State.Triggered),
                (State.Triggered, State.Alerted),
                (State.Triggered, State.Opened),
                (State.Alerted, State.Opened)
              ]


class StateMachine():
    callbacks = []
    state = State.Opened

    def __init__(self, logger):
        self.logger = logger

    def transition(self, state: AlarmState):
        if state == self.state:
            return

        if not [e for e in transitions if e == (self.state, state)]:
            raise RuntimeError("Invalid transition: {} -> {}".format(self.state, state))

        self.logger.info("Transition: {} -> {}".format(self.state, state))
        self.state = state

        for e in self.callbacks:
            e(state)

    @property
    def Current(self):
        return self.state

    def register(self, callback):
        self.callbacks.append(callback)
        callback(self.state)
