import time
import json
from threading import Thread
from flask import Flask, request
from flask_restful import Resource, Api
from .state_handler import State
from . import config

class Endpoint(Resource):
    state = None
    last_change = None
    last_signal = None
    app = Flask("Fina")

    def __init__(self, lock_impl, unlock_impl):
        self.lock_impl = lock_impl
        self.unlock_impl = unlock_impl

    def get(self):
        content = {
                    'state': self.state.Name,
                    'ts': int(self.last_change),
                    'last_signal': int(self.last_signal) if self.last_signal else None
                  }

        return json.dumps(content)

    def on_state_change(self, state):
        self.state = state.value
        self.last_change = time.time()

    def on_signal(self):
        self.last_signal = time.time()

    def start_impl(self):
        @self.app.route('/')
        def query_handler():
            return self.get()

        @self.app.route('/lock')
        def lock():
            self.lock_impl()
            return self.get()

        @self.app.route('/unlock')
        def unlock():
            self.unlock_impl()
            return self.get()

        self.app.run(host=config.host, port=config.port)

    def start(self):
        self.thread = Thread(target=Endpoint.start_impl, args=[self])
        self.thread.start()
