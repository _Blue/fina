#! /usr/bin/python3

import click
import requests
import time
import smtplib
import traceback
import logging
import socket
import backoff
import subprocess
from email.mime.text import MIMEText

TIME_FORMAT = "%Y-%m-%d %H:%M:%S"

@click.command()
@click.argument("uri")
@click.argument("smtp")
@click.argument("source")
@click.argument("target")
@click.option("--refresh", default=60)
@click.option("--logfile", default=None)
@click.option("--state-change-hook", default=None)
def monitor(uri, smtp, source, target, refresh, logfile, state_change_hook):
    setup_logging(logfile)

    logging.info("Started fina monitoring. Target URI: " + uri)

    status = read_status(uri)

    while True:
        new_status = read_status(uri)

        if new_status['state'] != status['state']:
            on_status_change(status, new_status, smtp, source, target, state_change_hook)

            status = new_status

        time.sleep(refresh)

def setup_logging(filename: str):
    handlers = [logging.StreamHandler()]

    if filename:
        handlers.append(logging.FileHandler(filename))


    logging.basicConfig(format='%(asctime)s %(message)s',
                        datefmt=TIME_FORMAT,
                        handlers=handlers,
                        level=logging.INFO)

@backoff.on_exception(backoff.constant, requests.exceptions.ConnectionError, max_tries=10, interval=5)
def read_status_impl(uri: str):
    response = requests.get(uri, timeout=15)
    if response.status_code != 200:
        raise RuntimeError("Endpoint returned status code: {}".format(response.status_code))

    try:
        return response.json()
    except Exception as e:
        raise RuntimeError("Failed to parse body as json: {}. Body: {}".format(e, response.text))

def read_status(uri: str):
    try:
        return read_status_impl(uri)

    except Exception as e:
        logging.warning("Failed to read fina's status: " + traceback.format_exc())
        return {
                "state": "Offline",
                "ts": int(time.time()),
                "extra": traceback.format_exc()
               }

def on_status_change(old_status, status, smtp, source, target, state_change_hook):
    logging.info("Status change {} -> {}".format(old_status["state"], status["state"]))

    if status["state"] in ["Alerted", "Offline"]:
        notify(status, smtp, source, target)

    if state_change_hook:
        cmd = "{} {} {} {}".format(state_change_hook, old_status["state"], status["state"], status["ts"])
        logging.info("Calling state change hook. Command line: " + cmd)
        subprocess.call(cmd, shell=True)


def get_mail_content(status):
    return """Hello,


This is fina monitor, running on {}.

**Alert**: entered {} state at: {}.

Extra infos: {}

sincerely,

--
Fina""".format(socket.getfqdn(),
        status["state"],
        time.strftime(TIME_FORMAT, time.gmtime(status["ts"])),
        status.get("extra", "none"))

@backoff.on_exception(backoff.constant, TimeoutError, max_tries=10, interval=10)
def notify(status, smtp, source, target):
    mail = MIMEText(get_mail_content(status))
    mail['From'] = source
    mail['To'] = target
    mail['X-Priority'] = '1'
    mail['X-MSMail-Priority'] = 'High'
    mail['Importance'] = 'High'
    mail['Subject'] = "[FINA] Entered %s state" % status['state']

    connection = smtplib.SMTP(smtp)
    connection.ehlo()
    connection.starttls()
    connection.ehlo()
    connection.sendmail(source, [target], mail.as_string())


    logging.info("Notification email successfully sent")


if __name__ == '__main__':
    monitor()
