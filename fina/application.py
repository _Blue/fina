#! /usr/bin/python3

import os
import sys
import time
import signal
import logging
import logging.handlers
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from threading import Thread, Timer
from . import config
from .frontend import FrontEnd
from .tty_frontend import TTYFrontend
from .state_handler import State, StateMachine
from .endpoint import Endpoint
from .sensor import Sensor
from .leds import Leds

class Application:
    timer = None
    leds = Leds()
    logger = logging.getLogger("fina")
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.handlers.SysLogHandler("/dev/log"))
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.addHandler(console)

    state_machine = StateMachine(logger)

    def __init__(self):
        if config.tty_mode:
            self.frontend = TTYFrontend(self.on_password_input, self.lock)
            self.logger.info("Starting fina in tty mode")
        else:
            self.app = QApplication(sys.argv)
            self.frontend = FrontEnd(self.on_password_input, self.lock, sys.argv)
            self.logger.info("Starting fina in X11 mode")

        signal.signal(signal.SIGINT, self.on_sigint)


        self.endpoint = Endpoint(lambda: self.lock(), lambda: self.on_password_input(config.password))
        self.sensor = Sensor(lambda: self.on_signal())
        self.state_machine.register(lambda state: self.frontend.on_state_change(state))
        self.state_machine.register(lambda state: self.endpoint.on_state_change(state))
        self.state_machine.register(lambda state: self.leds.on_state_change(state))

    def run(self):
        self.endpoint.start()
        self.sensor.start()
        self.leds.start()
        self.frontend.run()

    def on_sigint(self, signal, frame):
        if self.state_machine.Current != State.Opened:
            self.logger.error("Can't exit when not in Opened state")
        else:
            self.logger.info("Exit after receiving signal: {}".format(signal))
            os._exit(1)

    def on_signal(self):
        self.endpoint.on_signal()

        if self.state_machine.Current != State.Locked:
            return

        self.state_machine.transition(State.Triggered)
        self.frontend.on_timeout_change(config.alert_timeout)

        self.start_transition_timer(State.Alerted, time.time() + config.alert_timeout)


    def start_transition_timer(self, target, timeout: int):
        args = [self, self.state_machine.Current, target, timeout]

        self.timer = Timer(1, Application.tick, args=args)
        self.timer.start()


    def lock(self):
        # If enter is pressed while locking, then cancel lock
        if self.state_machine.Current == State.Locking:
            self.cancel_lock()
            self.state_machine.transition(State.Opened)
            return

        # Don't do anything if we're not in the correct state
        if self.state_machine.Current != State.Opened or self.timer:
            return

        self.state_machine.transition(State.Locking)
        self.frontend.on_timeout_change(config.lock_timeout)

        self.start_transition_timer(State.Locked, time.time() + config.lock_timeout)

    def cancel_lock(self):
        assert self.timer

        self.timer.cancel()
        self.timer = None

    def tick(self, original_state, target_state, timeout: int):
        assert self.timer

        # If state has changed during tick, cancel timer
        if self.state_machine.Current != original_state:
            self.cancel_lock()
        now = time.time()
        if now < timeout:

            self.frontend.on_timeout_change(round(timeout - now))
            self.start_transition_timer(target_state, timeout)
            return

        self.frontend.on_timeout_change(0)
        self.state_machine.transition(target_state)
        self.cancel_lock()


    def on_password_input(self, password: str):
        # If we receive an input while locking, cancel locking
        if self.state_machine.Current == State.Locking:
            self.cancel_lock()
            self.state_machine.transition(State.Opened)
            return

        if password == config.password:
            if self.state_machine.Current == State.Triggered:
                self.cancel_lock()
            self.state_machine.transition(State.Opened)
        elif self.state_machine.Current in [State.Locked, State.Alerted, State.Triggered]:
            self.logger.error("Got bad password")

def main():
    Application().run()

if __name__ == '__main__':
    main()
