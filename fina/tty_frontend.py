import sys
import traceback

class TTYFrontend:
    def __init__(self, on_password_input, on_return):
        self.on_password_input = on_password_input
        self.on_return = on_return

    def run(self):
        while(True):
            try:
                input = sys.stdin.readline()[:-1]
                if input:
                    self.on_password_input(input)
                else:
                    self.on_return()
            except Exception as e:
                traceback.print_exc()

    def on_state_change(self, state):
        pass

    def on_timeout_change(self, timeout: int):
        print(str(timeout))
