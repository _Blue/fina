#! /usr/bin/python3

import sys
from .import config
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class FrontEnd(QWidget):
    timeout = ""
    lock_released = False
    show_input = False

    def __init__(self, on_password_input, on_return, argv):
        self.app = QApplication(argv)
        super().__init__()

        self.on_password_input = on_password_input
        self.on_return = on_return
        self.show_input = False
        self.lock_released = True
        self.timeout = ""

        self.setWindowTitle('Fina')
        self.setWindowFlags(self.windowFlags() | Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowCloseButtonHint)

        self.label = QLabel('Unlocked', self)
        self.label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.label.setAlignment(Qt.AlignCenter)

        font = self.label.font()
        font.setPointSize(config.text_size)
        font.setBold(True)
        self.label.setFont(font)

        self.input = QLineEdit(self)
        self.input.move(-500, -500) # Not the cleanest way to make it invisible, but works
        self.input.setVisible(False)
        self.input.returnPressed.connect(self.on_return_pressed)

        layout = QHBoxLayout()
        layout.addWidget(self.label)
        self.setLayout(layout)

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_Return and not self.show_input and not event.isAutoRepeat():
            if self.lock_released:
                self.on_return()
                self.lock_released = False
            else:
                self.lock_released = True

        return super().keyReleaseEvent(event)

    def run(self):
        self.show()

        return self.app.exec_()

    @pyqtSlot()
    def _update(self):
        palette = self.palette()
        palette.setColor(self.backgroundRole(), self.color)
        self.setPalette(palette)

        self.label.setText(self.text + str(self.timeout))

        self.input.clear()
        self.input.setVisible(self.show_input)

        if (self.show_input):
            self.input.setFocus()
        else:
            self.setFocus()

    def on_state_change(self, state):
        self.text = state.value.Name
        self.color = state.value.Color
        self.timeout = ""
        self.show_input = state.value.RequirePassword
        QMetaObject.invokeMethod(self, "_update")

    def on_timeout_change(self, timeout):
        if timeout == 0:
            self.timeout = ""
        else:
            self.timeout = " (%s)" % timeout

        QMetaObject.invokeMethod(self, "_update")

    def on_return_pressed(self):
        self.on_password_input(self.input.text())
        self.input.clear()

